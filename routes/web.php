<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('account/create/{id?}', 'AccountController@create_account');
Route::post('account/add', 'AccountController@create_account');
Route::get('account/delete', 'AccountController@delete_account');
Route::get('account/manage', 'AccountController@manage_account');
Route::get('transaction/cashdeposit/{id}', 'AccountController@cash_deposit_withdraw');
Route::get('transaction/cashwithdraw/{id}', 'AccountController@cash_deposit_withdraw');
Route::post('transaction/deposit_withdraw', 'AccountController@cash_deposit_withdraw');
