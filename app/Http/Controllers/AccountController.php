<?php
/**
 * Created by PhpStorm.
 * User: rajesh
 * Date: 30/8/19
 * Time: 9:12 PM
 */
namespace App\Http\Controllers;

use App\account;
use App\transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
/*
 * creating account upadting account
 */
    function create_account(Request $request){
        if($request->method()=='POST'){
            $data=$request->all();
            $id=$data['id'];
            unset($data['id']);
            unset($data['_token']);
            if($id){
            account::where('id',$id)->update($data);
            }else {
            $data['account_number']=rand(123456646744,999999999999);
            $data['is_active']=1;
            $data['is_delete']=0;
            $data['created_at']=Carbon::now();
                $account_id = account::insertGetId($data);
                if ($account_id) {
                   $transact['account_id']=$account_id;
                   $transact['deposit_type']=1;// 1=>'deposit' 2=>'widraw'
                   $transact['transaction_type']=1;// 1=>'cash' 2=>'cheque'
                   $transact['amount_remain']=0;
                   $transact['previous_amount']=0;
                   $transact['created_at']=Carbon::now();
                   transaction::insert($transact);
                }
            }
            return redirect(URLLINK_DOMAIN.'/account/manage');
        }else{
            if(isset($request->id)) {
                $id = base64_decode($request->id);
                $account_details = account::where('id', $id)->get()->toArray();
                $data =isset($account_details)?$account_details[0]:[];
            }else{
                $data=[];
            }

        return view('account.addedit',['data'=>$data]);
        }
    }
    function manage_account(Request $request){
        $query=account::where('is_delete',0)->orderby('id','DESC');
        if (stristr($request->getrequestUri(), 'page=') == true) {
            $query_url = str_replace(substr($request->getrequestUri(), strrpos($request->getrequestUri(), '&page=')), '', $request->getrequestUri());
        } else {
            $query_url = $request->getrequestUri();
        }
        $data=$query->paginate(20)->withpath($query_url);
//        dd($data);
        return view('account.manageaccount')->with(compact('data'));
    }
    function delete_account(Request $request){
        $id=base64_decode($request->id);
        account::where('id',$id)->update(['is_delete'=>1,'is_active'=>0]);
        return redirect(URLLINK_DOMAIN.'/account/manage');
    }
    function cash_deposit_withdraw(Request $request){

        if($request->method()=='POST'){
            $data=$request->all();
            $prev_amt=transaction::where('account_id',$data['account_id'])->orderby('created_at',"DESC")->value('amount_remain');
            $transact['account_id']=$data['account_id'];;
            $transact['deposit_type']=$data['deposit_type'];// 1=>'deposit' 2=>'widraw'
            $transact['transaction_type']=$data['transaction_type'];// 1=>'cash' 2=>'cheque'
            $transact['amount_remain']=($data['deposit_type']==1)?$prev_amt+$data['amount']:$prev_amt-$data['amount'];
            $transact['previous_amount']=($prev_amt)?$prev_amt:'0';
            $transact['created_at']=Carbon::now();
            if(($prev_amt>$data['amount']) ||($data['deposit_type']==1))
            transaction::insert($transact);
            return redirect(URLLINK_DOMAIN.'/account/manage');
        }else{
            $id = base64_decode($request->id);
            $account_details = account::where('id', $id)->get()->toArray();
            $data =isset($account_details)?$account_details[0]:[];
            if (stristr($request->getrequestUri(), 'cashwithdraw') == true) {
                $data['transaction_type'] =1;
                $data['deposit_type'] =2;
            }else{
                $data['transaction_type'] =1;
                $data['deposit_type'] =1;
            }

            return view('transaction.cashdeposit')->with(compact('data'));
        }
    }
}