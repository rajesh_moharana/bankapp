@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Cash Deposit
                        <br>
                    </div>

                    <div class="card-body">
                        <form name="deposit" action="{{URLLINK_DOMAIN}}/transaction/deposit_withdraw" method="post">
                            @csrf
                            <input type="hidden" name="transaction_type" value="<?php if (old('transaction_type') != null) echo old('transaction_type');
                            else echo ($data['transaction_type'])?$data['transaction_type']:'';?>">
                            <input type="hidden" name="deposit_type" value="<?php if (old('deposit_type') != null) echo old('deposit_type');
                            else echo ($data['deposit_type'])?$data['deposit_type']:'';?>">

                            <input type="hidden" name="account_id" value="<?php if (old('account_id') != null) echo old('account_id');
                            else echo ($data['id'])?$data['id']:'';?>">
                            <div class="form-group {{ $errors->has('amount') ? ' has-error' : '' }}">
                                <label>Amount&nbsp;&nbsp;</label>

                                <input type="text"  name="amount" value="<?php if (old('amount') != null) echo old('amount');
                                else echo isset($data['amount'])?$data['amount']:'';?>">
                                @if ($errors->has('amount')) <span
                                        class="help-block"> <strong>{{ $errors->first('amount') }}</strong> </span> @endif
                            </div>
                            <button type="submit">Save</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
