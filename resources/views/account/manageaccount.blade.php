@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Manage Account
                        <br>
                        <a href="{{URLLINK_DOMAIN}}/account/create">Craete Account</a>
                    </div>

                    <div class="card-body">
                        <table width="100%" border="1px lightgrey">
                            <tr>
                                <th>Holder Name</th>
                                <th>Account Number</th>
                                <th>Account Status</th>
                                <th>Address</th>
                                <th>Action</th>
                            </tr>
                            @foreach($data AS $key=>$val)
                                <?php             $prev_amt=App\transaction::where('account_id',$val->id)->orderby('created_at',"DESC")->value('amount_remain');
                                ?>
                                <tr>
                                    <td>{{$val->holder_name}}</td>
                                    <td>{{$val->account_number}}</td>
                                    <td>{{($val->is_active==1)?'Activated':'Deactivated'}}</td>
                                    <td>{{$val->Address}}</td>
                                    <td>
                                        <a href="{{URLLINK_DOMAIN}}/account/create/{{base64_encode($val->id)}}">Edit</a>
                                        |<a href="{{URLLINK_DOMAIN}}/account/delete/{{base64_encode($val->id)}}">Delete</a>
                                       | <a href="{{URLLINK_DOMAIN}}/transaction/cashdeposit/{{base64_encode($val->id)}}">Depost</a>
                                        |@if(!empty($prev_amt))
                                            <a href="{{URLLINK_DOMAIN}}/transaction/cashwithdraw/{{base64_encode($val->id)}}">Withdraw</a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
