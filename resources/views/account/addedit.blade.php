@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add Account</div>

                <div class="card-body">
                   <form name="account_creation" id="account_creation" method="post" action="{{URLLINK_DOMAIN}}/account/add">
                      <input type="hidden" name="id" value="<?php echo isset($data['id'])?$data['id']:'';?>">
                       @csrf
                       <div class="form-group {{ $errors->has('holder_name') ? ' has-error' : '' }}">
                           <label>Holder's Name&nbsp;&nbsp;</label>

                       <input type="text"  name="holder_name" value="<?php if (old('holder_name') != null) echo old('holder_name');
else echo isset($data['holder_name'])?$data['holder_name']:'';?>">
                    @if ($errors->has('holder_name')) <span
                            class="help-block"> <strong>{{ $errors->first('holder_name') }}</strong> </span> @endif
                       </div>

                       <div class="form-group {{ $errors->has('fathers_name') ? ' has-error' : '' }}">
                           <label>Father's Name&nbsp;&nbsp;</label>

                           <input type="text"  name="fathers_name" value="<?php if (old('fathers_name') != null) echo old('fathers_name');
                           else echo isset($data['fathers_name'])?$data['fathers_name']:'';?>">
                           @if ($errors->has('fathers_name')) <span
                                   class="help-block"> <strong>{{ $errors->first('fathers_name') }}</strong> </span> @endif
                       </div>

                       <div class="form-group {{ $errors->has('mothers_name') ? ' has-error' : '' }}">
                           <label>Mother's Name&nbsp;&nbsp;</label>

                           <input type="text"  name="mothers_name" value="<?php if (old('mothers_name') != null) echo old('mothers_name');
                           else echo isset($data['mothers_name'])?$data['mothers_name']:'';?>">
                           @if ($errors->has('mothers_name')) <span
                                   class="help-block"> <strong>{{ $errors->first('mothers_name') }}</strong> </span> @endif
                       </div>

                       <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                               <label>Address &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <textarea name="address"><?php if (old('address') != null) echo old('address');
                                else echo isset($data['address'])?$data['address']:'';?></textarea>
                           @if ($errors->has('address')) <span
                                   class="help-block"> <strong>{{ $errors->first('address') }}</strong> </span> @endif
                       </div>
                       <input type="submit" value="Submit">
                       <input type="button" value="cancel" onclick="window.location='{{URL::previous()}}'">
                   </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
